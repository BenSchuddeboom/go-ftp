package ftp

import (
	"bufio"
	"fmt"
	"log"
	"runtime"
	"strings"
)

// We will now listen for input and respond to it
func serve(c *Conn) {
	c.write(statusReady)

	s := bufio.NewScanner(c.conn)
	for s.Scan() {
		fields := strings.Fields(s.Text())
		op, args := fields[0], fields[1:]

		log.Printf("[IN] - %s", fields)

		switch op {
		case "USER":
			c.write(statusLoggedIn)
		case "QUIT":
			c.write(statusClose)
		case "SYST":
			c.write(fmt.Sprintf(statusSystemType, runtime.GOOS))
		case "PORT":
			c.port(args)
		case "PWD":
			c.pwd()
		case "MKD":
			c.mkdir(args[0])
		case "LIST":
			c.list(args)
		case "CWD":
			c.cwd(args)
		default:
			c.write(statusBadCommand)
		}
	}
}
