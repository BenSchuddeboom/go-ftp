package ftp

import (
	"fmt"
	"io/ioutil"
	"log"
	"net"
	"os"
	"strings"
)

// Conn is a wrapper around net.conn
type Conn struct {
	server     *Server
	conn       net.Conn
	dataPort   *dataPort
	currentDir string
}

func newConn(c net.Conn, s *Server) *Conn {
	return &Conn{
		server:     s,
		conn:       c,
		dataPort:   nil,
		currentDir: s.workDir,
	}
}

func (c *Conn) write(msg string) {
	_, err := fmt.Fprint(c.conn, msg, "\r\n")
	if err != nil {
		log.Print(err)
		return
	}
	log.Printf("[OUT] - %s", msg)
}

func (c *Conn) setDataPort(plainDataPort string) error {
	var d dataPort
	_, err := fmt.Sscanf(plainDataPort, "%d,%d,%d,%d,%d,%d", &d.h1, &d.h2, &d.h3, &d.h4, &d.p1, &d.p2)
	if err != nil {
		return err
	}
	c.dataPort = &d
	return nil
}

func (c *Conn) pwd() {
	wd, err := os.Getwd()
	if err != nil {
		log.Print(err)
		c.write(statusFileUnavailable)
		return
	}
	c.write(wd)
}

func (c *Conn) mkdir(name string) {
	if len(name) == 0 {
		c.write(statusBadCommandForParam)
		return
	}
	err := os.Mkdir(name, os.ModeDir)
	if err != nil {
		c.write(statusFileUnavailable)
		return
	}
	c.write(statusActionOK)
}

func (c *Conn) port(args []string) {
	err := c.setDataPort(args[0])
	if err != nil {
		c.write(statusFailedDataConnection)
	}
	c.write(statusOK)
}

func (c *Conn) dataPortConnect() (net.Conn, error) {
	dpConn, err := net.Dial("tcp", c.dataPort.getAddress())
	if err != nil {
		return nil, err
	}
	return dpConn, nil
}

// Supports only abs paths atm
func (c *Conn) list(args []string) {
	var dirname string
	if len(args) > 0 {
		dirname = args[0]
	} else {
		dirname = c.currentDir
	}

	files, err := ioutil.ReadDir(dirname)
	if err != nil {
		c.write(statusError)
		log.Print(err)
		return
	}

	c.write(statusOpenDataConn)

	dpConn, err := c.dataPortConnect()
	if err != nil {
		c.write(statusFailedDataConnection)
	}
	defer dpConn.Close()

	for _, f := range files {
		_, err := fmt.Fprint(dpConn, f.Name(), "\r\n")
		if err != nil {
			c.write(statusAborted)
			log.Print(err)
		}
	}

	_, err = fmt.Fprintf(dpConn, "\r\n")
	if err != nil {
		log.Print(err)
		c.write(statusAborted)
	}

	c.write(statusTransferSuccesful)
}

func (c *Conn) cwd(args []string) {
	if len(args) == 0 {
		c.write(c.currentDir)
		return
	}

	var path string

	if strings.HasPrefix(args[0], "/") {
		path = args[0]
	} else {
		path = c.currentDir + "/" + args[0]
	}

	err := os.Chdir(path)
	if err != nil {
		log.Print(err)
		c.write(statusBadCommandForParam)
		return
	}

	c.currentDir = path
	c.write(statusActionOK)
}
