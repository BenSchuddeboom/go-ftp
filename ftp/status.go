package ftp

// Status codes with this formatting are not optional for talking with any established FTP client.
// Client behaviour by for example the ftp CLI is partially determined by server status codes, even more so than with http.
const (
	statusOpenDataConn         = "150 File status okay; about to open data connection."
	statusOK                   = "200 Okay."
	statusSystemType           = "215 %s system type."
	statusReady                = "220 Service ready for new user."
	statusClose                = "221 Service closing control connection."
	statusTransferSuccesful    = "226 Closing data connection. Action succesful."
	statusLoggedIn             = "230 User logged in."
	statusActionOK             = "250 Requested file action okay, completed."
	statusFailedDataConnection = "425 Can't open data connection."
	statusAborted              = "426 Connection closed. Transfer aborted."
	statusError                = "500 An error occurred."
	statusSyntaxError          = "501 Syntax error in parameters or arguments."
	statusBadCommand           = "502 Command not implemented."
	statusBadCommandForParam   = "504 Command not implemented for that parameter."
	statusFileUnavailable      = "550 Requested action not taken. File unavailable."
)
