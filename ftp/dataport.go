package ftp

import "fmt"

type dataPort struct {
	h1, h2, h3, h4 int
	p1, p2         int
}

func (d *dataPort) getAddress() string {
	return fmt.Sprintf("%d.%d.%d.%d:%d", d.h1, d.h2, d.h3, d.h4, d.p1*256+d.p2)
}
