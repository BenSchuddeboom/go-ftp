package ftp

import (
	"log"
	"net"
	"os"
)

type Server struct {
	workDir string
}

func newServer(workDir string) *Server {
	return &Server{
		workDir: workDir,
	}
}

func NewServer(workDir string) *Server {
	return newServer(workDir)
}

// ListenAndServe the main entry point for the package
func (s *Server) ListenAndServe(addr string) {
	l, err := net.Listen("tcp", addr)
	if err != nil {
		log.Fatal(err)
	}

	err = os.Chdir(s.workDir)
	if err != nil {
		log.Fatal("Cannot use specified directory as workDir.")
	}

	log.Printf("Server started on %s", addr)
	defer l.Close()
	for {
		c, err := l.Accept()
		if err != nil {
			log.Print(err)
			continue
		}
		defer c.Close()
		go handleConn(c, s)
	}
}

func handleConn(c net.Conn, s *Server) {
	defer c.Close()

	// wrapping net.Conn
	conn := newConn(c, s)
	serve(conn)
}
