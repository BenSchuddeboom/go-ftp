module gitlab.com/BenSchuddeboom/go-ftp

go 1.15

require (
	github.com/withmandala/go-log v0.1.0 // indirect
	go.uber.org/multierr v1.6.0 // indirect
	go.uber.org/zap v1.16.0
	golang.org/x/crypto v0.0.0-20201221181555-eec23a3978ad // indirect
)
